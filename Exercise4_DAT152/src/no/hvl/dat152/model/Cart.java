package no.hvl.dat152.model;

import no.hvl.dat152.utils.StringUtil;

import javax.ejb.Remote;
import java.util.*;

@Remote
public class Cart {
	//private List<Product> items;
	private Map<Product, Integer> items;
	private int total;
	
	public Cart() {
		items = new WeakHashMap<>();
		total = 0;
	}
	
	public void addItem(Product item) {
	    Integer prevVal = 0;
	    if(items.containsKey(item)) {
			prevVal = items.get(item);
		}

		items.put(item, ++prevVal);

	    total += item.getPriceInEuro();
	}

	public List<Product> getItems() {
		List<Product> list = new ArrayList<>();
		list.addAll(this.items.keySet());

		return list;
	}

	public Integer getQuantity(Product item) {
		return items.get(item);
	}

	public int getTotal() {
		return total;
	}

	// remove osv...
}
