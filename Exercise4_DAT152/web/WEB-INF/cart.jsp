<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="dat152-libs" prefix="dat152" %>
<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Cart</title>
  </head>
  <body>
    <fmt:setLocale value="${ currentLocale }"/>
    <dat152:localeSwitcher locales="${ locales }"/>
    <fmt:setBundle basename="apptexts" var="app"/>
    <fmt:setBundle basename="descriptions" var="des"/>
    <h2><fmt:message key="cart" bundle="${ app }"/></h2>
    <%-- print cart table --%>
    <table style="border: 1px solid black; /*border-collapse: collapse;*/">
      <thead>
        <tr>
          <fmt:bundle basename="apptexts">
            <th><fmt:message key="name"/></th>
            <th><fmt:message key="shortDescription"/></th>
            <th><fmt:message key="price"/></th>
            <th><fmt:message key="quantity"/></th>
            <th><fmt:message key="total"/></th>
          </fmt:bundle>
        </tr>
      </thead>
      <tbody>
        <fmt:bundle basename="apptexts">
          <c:forEach items="${ items }" var="item">
            <tr>
              <td>${ item.getpName() }</td>
              <td>
                <dat152:shorttext maxchars="10">
                  <fmt:message key="${ item.getPno() }" bundle="${ des }"/>
                </dat152:shorttext>
              </td>
              <td>
                <fmt:formatNumber type="currency">
                  <dat152:currencyConverter toCurrency="${ currency }" amount="${ item.getPriceInEuro() }"/>
                </fmt:formatNumber>
              </td>
              <td>${ cart.getQuantity(item) }</td>
              <td>
                <c:set var="convertedValue">
                  <dat152:currencyConverter toCurrency="${ currency }" amount="${ item.getPriceInEuro() }"/>
                </c:set>
                <fmt:formatNumber type="currency">
                  ${ convertedValue * cart.getQuantity(item) }
                </fmt:formatNumber>
              </td>
            </tr>
          </c:forEach>
        </fmt:bundle>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="4"><fmt:message key="totalAmount" bundle="${ app }"/></td>
          <td>
            <fmt:formatNumber type="currency">
              <dat152:currencyConverter toCurrency="${ currency }" amount="${ cart.getTotal() }"/>
            </fmt:formatNumber>
          </td>
        </tr>
      </tfoot>
    </table>
    <%-- /print cart table --%>
    <a href="home"><fmt:message key="home" bundle="${ app }"/></a>
    <a href="products"><fmt:message key="products" bundle="${ app }"/></a>
  </body>
</html>