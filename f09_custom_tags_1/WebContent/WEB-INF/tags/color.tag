<%@ tag language="java" pageEncoding="UTF-8"%>
<%@ attribute name="bgcolor" type="String" %>
<%@ attribute name="fgcolor" type="String" %>

<table style="background-color: ${ bgcolor }; color: ${ fgcolor }" border="1">
	<tr>
		<th><h2><jsp:doBody/></h2></th>
	</tr>
</table>