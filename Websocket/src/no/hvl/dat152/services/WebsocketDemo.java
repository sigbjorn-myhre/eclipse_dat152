package no.hvl.dat152.services;


import java.io.IOException;

import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/demo") 
public class WebsocketDemo {

	@OnOpen
    public void onOpen(Session session) {
        System.out.println("Client " + session.getId() + " has opened a connection.");
    }

    @OnMessage
    public void onMessage(String message, Session session) throws IOException {
    	System.out.println("Got message from " + session.getId()+": " + message);
    	try {
			session.getBasicRemote().sendText("Data sent from server to client");
		} catch (IOException e) {
			e.printStackTrace();
		}

    }
    
    @OnClose
    public void onClose(Session session){
        System.out.println("Client " +session.getId()+" has ended");
    }
}
