package no.hib.dat152.bitofeverything;

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;

import no.hib.dat152.model.KelvinToString;
import no.hib.dat152.model.TodayData;

/**
 * The main app.
 *
 * @author Atle Geitung
 * @author Lars-Petter Helland
 */
public final class Main {

    /**
     * Hides the constructor.
     */
    private Main() {
    }

    /**
     * The main app.
     *
     * @param args Not used.
     * @throws InstantiationException fail
     * @throws IllegalAccessException fail
     * @throws ClassNotFoundException fail
     */
    public static void main(final String[] args) throws InstantiationException, IllegalAccessException,
    ClassNotFoundException {

        // Locale locale = Locale.getDefault();
        // Locale locale = new Locale("en");

        Locale locale = new Locale("no");
        KelvinToString kelvinToString;
        ResourceBundle apptexts = ResourceBundle.getBundle("apptexts", locale);
        DateFormat dateFormat = DateFormat.getDateInstance(DateFormat.LONG, locale);

        // Alt.1
        System.out.println("Alternative 1");

        ResourceBundle appobjects = ResourceBundle.getBundle("no.hib.dat152.model.LocaleSensitiveLogic", locale);
        kelvinToString = (KelvinToString) appobjects.getObject("tempformat");

        System.out.println("Chosen locale: " + locale);
        System.out.println("Texts locale: " + apptexts.getLocale());
        System.out.println("Temps locale: " + appobjects.getLocale());

        System.out.print(apptexts.getString("todaysNews") + " ");
        System.out.println(dateFormat.format(new Date()) + ".");

        System.out.print(apptexts.getString("maxTodayWas") + " ");
        System.out.println(kelvinToString.format(TodayData.getMaxTemp()) + ".");

        // Alt.2
        System.out.println("Alternative 2");

        String tempFormatClass = apptexts.getString("tempFormat");
        kelvinToString = (KelvinToString) Class.forName(tempFormatClass).newInstance();
        kelvinToString.setLocale(locale);

        System.out.println("Chosen locale: " + locale);
        System.out.println("Texts locale: " + apptexts.getLocale());

        System.out.print(apptexts.getString("todaysNews") + " ");
        System.out.println(dateFormat.format(new Date()) + ".");

        System.out.print(apptexts.getString("maxTodayWas") + " ");
        System.out.println(kelvinToString.format(TodayData.getMaxTemp()) + ".");

        // Alt.3
        /*
         * Hverken i Alt.1 eller Alt.2 er KelvinToString en lokaliserings-sensitiv klasse. I begge tilfeller har man
         * valgt korrekt implementasjon av KelvinToString indirekte (Alt.1 via en ListResourceBundle og Alt.2 via en
         * property i en PropertyResourceBundle).
         *
         * Hvis man �nsker en lokaliserings-sensitiv KelvinToString, m� denne gj�res om til en abstrakt klasse og
         * forsynes med en lokale-sensitiv factory-metode som returnerer korrekt subklasse.
         *
         * Dette blir litt mindre fleksibelt enn Alt.1 og Alt.2 siden vi m� endre factory-metoden hvis vi �nsker flere
         * spr�k (brudd p� OCP). I Alt.1 og Alt.2 legger vi kun til ting, vi endrer ikke eksisterende.
         */

    }
}
