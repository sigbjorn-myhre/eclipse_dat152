/**
 * 
 */
class AppController {
    constructor(memberlist,addMember) {
      this.memberlist = memberlist
      this.addMember = addMember
    }

    run() {
      this.ui = new UIHandler()
      document.getElementById(this.memberlist).appendChild(this.ui.UIElement)
      
      ui.deleteMemberCallback = this.deleteMemberAJAX()
      ui.editMemberCallback = this.editMemberAJAX()

      // this.ui.addMember({"id":2,"firstname":"Per","lastname":"Persen","address":"Persenbakken 77","phone":"14546567"})
      // this.ui.addMember({"id":1,"firstname":"Ole","lastname":"Olsen","address":"Olsenbakken","phone":"91826453"})
    }
    
    // Metode som kjøres når 'delete' trykkes
    deleteMemberAJAX() {
    	const xhr = new XMLHttpRequest()
    	xhr.open('DELETE', () => {
    		
    	}, true)
    }
    
    // Metode som kjøres når 'edit' trykkes
    editMemberAJAX() {
    	
    }
}

const app = new AppController('memberlist', 'addMember')
document.addEventListener('DOMContentLoaded',app.run.bind(app),true)