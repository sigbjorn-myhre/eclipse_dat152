<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="dat152-libs" prefix="dat152" %>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Home</title> <%-- internasjonalisere denne også? --%>
	</head>
	<body>
    <fmt:setLocale value="${ currentLocale }"/><%-- TEMP TEMP TEMP --%>
  	<dat152:localeSwitcher locales="${ locales }"/>
    <fmt:bundle basename="apptexts">
      <h2><fmt:message key="homeTitle"/></h2>
            <img src="img/logo.jpg">
			<p><fmt:message key="greeting"/> <a href="products"><fmt:message key="products"/></a></p>
		</fmt:bundle>
		<p><dat152:copyright since="2013">Høgskolen på Vestlandet</dat152:copyright></p>
	</body>
</html>